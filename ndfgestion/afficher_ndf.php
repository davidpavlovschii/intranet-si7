<?php
    if ($_SESSION['typeCompte']=='Gestionnaire') {
?>
        <h3>Note de frais à valider</h3>
    	<label>Note de frais : </label>
<?php
    	if(isset($_SESSION['idUser']))
    	{
    		$arrayNDF = AfficherNDF();
    		while($ndf = mysqli_fetch_array($arrayNDF))
    		{
?>
                <table>
                    <form action='index.php?menu=traitement_ndf&noNdf=<?php echo $ndf['noNdf'] ?>' method="POST">
                        <button type="submit" formmethod="post" name="Refuser">Refuser</button>
                        <button type="submit" formmethod="post" name="Valider">Valider</button>
                    </form> 
                    <tr>
                        <td><?php echo 'No :'; ?></td>
                        <td><a href='index.php?menu=detail_ndf&noNdf=<?php echo $ndf['noNdf'] ?>' method="GET"><?php print_r($ndf['noNdf']); ?></a></td>
                    </tr>
                    <tr>
                        <td><?php echo 'Nom :'; ?></td>
                        <td><?php print_r($ndf['nom']); ?></a></td>
                    </tr>
                    <tr>
                        <td><?php echo 'Prenom :'; ?></td>
                        <td><?php print_r($ndf['prenom']); ?></td>	
                    </tr>
                    <tr>
                        <td><?php echo 'Date de creation :'; ?></td>
                        <td><?php print_r($ndf['dateNDF']); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo 'Prix en Euros :'; ?></td>
                        <td><?php print_r($ndf['somme']); ?></td>
                    </tr>
                    <br>
                </table>
<?php
            }
    	}
    }
    else
    {
    	echo "Vous n'avez pas accès à cette page";
        echo "<meta http-equiv='refresh' content='3;url=../login.php'>";
    }
?>
