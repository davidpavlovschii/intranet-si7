<?php
	if ($_SESSION['typeCompte']=='Employé') {
?>
		<h3>Création d'une nouvelle dépense</h3>
		<form method="post" action="?menu=ajout_depense">
			
			<label>Note de frais : </label>
			<select name="choix_ndf">
			<?php
			if(isset($_SESSION['idUser']))
			{
				$arrayNDF = chargerNDFdeUser($_SESSION['idUser']);
				while($ndf = mysqli_fetch_array($arrayNDF))
				{
					echo '<option value='.$ndf["numNDF"].'>'.$ndf["formatDateNDF"].'</option>';
				}
			}
			else
			{
				header('location: login.php');
			}
			?>
			</select><br>

			<label>Intitulé de la dépense : </label>
			<input type="text" name="titre_depense"><br>

			<label>Montant de la dépense : </label>
			<input type="text" name="montant_depense"><br>

			<br><button type="submit">Créer nouvelle dépense</button>
		</form>
<?php
	}

	else {
		echo "Vous n'avez pas accès à cette page";
		echo "<meta http-equiv='refresh' content='3;url=index.php?menu=NDFgestion'>";
	}
?>