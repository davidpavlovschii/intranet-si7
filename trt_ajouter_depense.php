<?php
if(isset($_SESSION['idUser']))
{
    if(isset($_POST["choix_ndf"]) &&  isset($_POST["titre_depense"]) && isset($_POST["montant_depense"]))
    {
        if(add_depense($_POST["titre_depense"],$_POST["montant_depense"],$_POST["choix_ndf"]))
        {
            echo 'Dépense ajouté.';
        }
        else
        {
            echo "Erreur lors de l'ajout de la dépense";
        }
    }
    else
    {
        echo "Veuillez remplir tous les champs";
    }
}
else
{
	header('location:login.php');
}
?>