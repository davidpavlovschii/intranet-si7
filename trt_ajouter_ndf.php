<?php
if(isset($_SESSION['idUser']))
{
	if(verif_exist_ndf($_SESSION['idUser']))
	{
		if(create_ndf($_SESSION['idUser']))
		{
			echo "NDF ajouté.";
		}
		else
		{
			echo "Erreur lors de l'ajout de la NDF.";
		}
	}
	else
	{
		echo "Une NDF existe déjà pour cette date.";
	}
}
else
{
	header('location:login.php');
}
?>