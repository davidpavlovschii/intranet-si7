﻿<?php
	function enTeteHTML($nomCSS){
		echo "
			<!doctype html>
			<html>
				<head>
					<meta charset='UTF-8'>
					<link rel='stylesheet' type='text/css' href='".$nomCSS."'/>
				</head>
				<body>
		";
	}
	
	function piedDePageHTML(){
		echo "
				</body>
			</html>
		";
	}
	
	//se connecte à la base de données
	//retourne la variable de connexion
	function bddConnect(){ 
		$con = mysqli_connect("localhost", "root", "", "ndfgestion");
		mysqli_set_charset($con, "utf8");
		return $con;
	}
	
	//vérifie qu'un utilisateur ayant le login $user et le mot de passe $mdp existe bien dans la base de données.
	//retourne ses informations (tables user et type_compte) si c'est le cas, et 0 dans le cas contraire
	function verifConnect($user, $mdp){
		$con = bddConnect();
		$req = "SELECT *
				FROM user, type_compte, service
				WHERE  user.numTC = type_compte.numTC
				AND user.numService = service.numService
				AND loginUser = '$user'
				AND mdpUser = '$mdp'";
		//récupère le nombre d'enregistrements de la table résultat
		$res = mysqli_query($con, $req);
		//si il y a 1 enregistrement dans la table résultat 
		//(ie : l'utilisateur qui a les login / mot de passe passés en paramètre a été trouvé)
		if (mysqli_num_rows($res)==1) {
			return mysqli_fetch_array($res);
		}
		else{
			return 0 ;
		}
		mysqli_close($con);
	}

	function verif_exist_ndf($user)
	{
		$today = date("Y-m-d");
		$con = bddConnect();
		$req = "SELECT * 
		FROM note_de_frais ndf, user u
		WHERE ndf.loginUser = u.loginUser
		AND ndf.loginUser = '$user'
		AND ndf.dateNDF = '$today'";
		$res = mysqli_query($con, $req);
		if(mysqli_num_rows($res)>=1)
		{
			return false;
		}
		return true;
	}

	function create_ndf($user)
	{
		$today = date("Y-m-d");
		$con = bddConnect();
		$req = 'INSERT INTO note_de_frais(dateNDF, loginUser, numEN) VALUES("'.$today.'","'.$_SESSION['idUser'].'",1)';
		$res = mysqli_query($con, $req);
		return $res;
	}

	function add_depense($libelleDep,$montantDep,$numNDF)
	{
		$con = bddConnect();
		$req = 'INSERT INTO depense(libelleDep, montantDep, numNDF) VALUES("'.$libelleDep.'","'.$montantDep.'","'.$numNDF.'")';
		$res = mysqli_query($con, $req);
		return $res;
	}

	function chargerNDFdeUser($user)
	{
		$con = bddConnect();
		$req = 'SELECT ndf.numNDF, DATE_FORMAT(ndf.dateNDF,"%d-%m-%Y") as formatDateNDF, ndf.loginUser, ndf.numEN 
		FROM user u, note_de_frais ndf
		WHERE u.loginUser = ndf.loginUser
		AND ndf.loginUser = "'.$user.'"';
		return $res = mysqli_query($con, $req);
	}
	function AfficherNDF()
	{
		$con = bddConnect();	
		$req = "SELECT SUM(d.montantDep) as somme, n.dateNDF as dateNDF, u.nomUser as nom, u.prenomUser as prenom, n.numNDF as noNdf
		FROM depense d, note_de_frais n, user u
		WHERE d.numNDF = n.numNDF
		AND n.loginUser = u.loginUser
		AND  numEN = 1
		GROUP BY n.numNDF
		ORDER BY n.dateNDF ASC, u.nomUser, u.prenomUser";

		return $res = mysqli_query($con, $req);
	}

	function detailNdf($noNdf)
	{
		$con = bddConnect();	
		$req = "SELECT d.libelleDep as libelle, d.montantDep as depense, n.dateNDF as dateNDF, u.nomUser as nom, u.prenomUser as prenom
		FROM depense d, note_de_frais n, user u
		WHERE d.numNDF = n.numNDF
		AND n.loginUser = u.loginUser
		AND  numEN = 1
        AND n.numNDF = $noNdf
		ORDER BY n.dateNDF ASC, u.nomUser, u.prenomUser";

		return $res = mysqli_query($con, $req);
	}

	function dateDetailNdf($noNdf)
	{
		$con = bddConnect();	
		$req = "SELECT SUM(d.montantDep) as somme, n.dateNDF as dateNDF
		FROM depense d, note_de_frais n, user u
		WHERE d.numNDF = n.numNDF
		AND n.loginUser = u.loginUser
		AND  numEN = 1
        AND n.numNDF = $noNdf
		ORDER BY n.dateNDF ASC, u.nomUser, u.prenomUser";

		return $res = mysqli_query($con, $req);
	}

	function validerNdf($noNdf)
	{
		$con = bddConnect();	
		$req = "UPDATE note_de_frais
		SET numEN = 2
		WHERE  numNDF = $noNdf";
		if (mysqli_query($con, $req) === TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function refuserNdf($noNdf)
	{
		$con = bddConnect();	
		$req = "UPDATE note_de_frais
		SET numEN = 3
		WHERE  numNDF = $noNdf";
		if (mysqli_query($con, $req) === TRUE)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
?>